#!/opt/bitnami/php/bin/php
<?php

namespace InterFix\PHPSMTPMailer;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

include_once __DIR__ . '/../vendor/autoload.php';

PHPSMTPMailer::main();
exit();


/**
 * Class PHPSMTPMailer
 * @package InterFix\PHPSMTPMailer
 */
class PHPSMTPMailer {
	
	/**
	 *
	 */
	public static function main() {
		
		try {
			
			// see https://www.php.net/manual/en/function.getopt.php
			$options = getopt( 's:t:f:n:v' );
			
			if ( ! $options ) {
				echo PHPSMTPMailer::getHelp() . "\n\n";
				exit();
			}
			
			$verbose  = isset( $options['v'] );
			$subject  = $options['s'] ?? 'Message Subject';
			$toAddr   = $options['t'] ?? null;
			$fromAddr = $options['f'] ?? null;
			$fromName = $options['n'] ?? null;
			
			if ( is_null( $toAddr ) ) {
				throw new Exception( 'Missing Recipient/To Address. Use option: -t user@server.com' );
			}
			
			if ( is_null( $fromName ) ) {
				$processUser = posix_getpwuid( posix_geteuid() );
				$fromName    = $processUser['name'];
			}
			
			if ( is_null( $fromAddr ) ) {
				throw new Exception( 'Missing Sender/From Address. Use option: -f user@server.com -n "Sender Name"' );
			}
			
			if ( $verbose ) {
				echo "\$subject: $subject\n";
				echo "\$verbose: " . ( $verbose ? 'Yes' : 'No' ) . "\n";
				echo "\$toAddr: $toAddr\n";
				echo "\$fromAddr: $fromAddr\n";
				echo "\$fromName: $fromName\n";
			}
			
			$fp = file( "php://stdin" );
			
			if ( self::arrayIsEmpty( $fp ) ) {
				
				if ( $verbose ) {
					echo "No message content. Aborting.\n";
				}
				
				exit();
			}
			
			$mail            = new PHPMailer();
			$mail->SMTPDebug = $verbose ? SMTP::DEBUG_SERVER : Config::PHPMAILER_SMTP_DEBUG_LEVEL;
			$mail->isSMTP();
			$mail->Host     = Config::SMTP_HOST;
			$mail->SMTPAuth = true;
			$mail->Username = Config::SMTP_USERNAME;
			$mail->Password = Config::SMTP_PASSWORD;
			if ( Config::SMTP_AUTH_METHOD === 'TLS' ) {
				$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
			}
			$mail->Port = Config::SMTP_PORT;
			
			$mail->setFrom( $fromAddr, $fromName );
			$mail->addAddress( $toAddr );
			#$mail->addReplyTo('info@example.com', 'Information');
			#$mail->addCC('cc@example.com');
			#$mail->addBCC('bcc@example.com');
			
			$mail->Subject = $subject;
			$mail->Body    = implode( '', $fp );
			
			if ( ! $mail->send() ) {
				throw new Exception( 'An error occurred when attempting to send the email: ' . $mail->ErrorInfo );
			}
			
			if ( $verbose ) {
				echo "Message has been sent.\n";
			}
			
		} catch ( Exception $e ) {
			echo "An error occurred while sending the message:\n\n    {$e->getMessage()}\n\n";
		}
	}
	
	/**
	 * @return string
	 */
	public static function getHelp() {
		
		$fileName = basename( __FILE__ );
		
		return "Usage:\npipe command output in this script with appropriate command line args.\n\n  php $fileName -s \"Message Subject\" -t to@example.com -f from@example.com [-n \"From Name\"] [-v]";
	}
	
	/**
	 * @param $a
	 *
	 * @return bool
	 */
	private static function arrayIsEmpty( $a ) {
		foreach ( $a as $item ) {
			if ( ( trim( $item ) === '0' ) || ! empty( trim( $item ) ) ) {
				return false;
			}
		}
		
		return true;
	}
}