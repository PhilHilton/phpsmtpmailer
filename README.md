# PHPSMTPMailer #

Utility script for sending emails through an SMTP server. Originally intended for use in redirecting cron job output to be emailed through Amazon SES on a Bitnami/Lightsail instances that are not configured to send mail by default.
